package dev.tucker;


public class Stack {
    private int n; // size of the stack
    private Node head;


    private class Node {
        Object value;
        Node next;
    }

    public Stack() {
        this.n = 0;
        this.head = null;
    }

    public void push(Object object) {
        Node oldHead = head;
        head = new Node();
        head.value = object;
        head.next = oldHead;
        n++;
    }

    public Object pop() {
        Node poppedHead = head;
        head = head.next;
        n--;
        return poppedHead.value;
    }

    public Object peek() {
        return head.value;
    }

    public boolean isEmpty() {
        return head == null;
    }

    public int size() {
        return n;
    }


}
