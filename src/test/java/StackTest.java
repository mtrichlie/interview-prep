import dev.tucker.Stack;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class StackTest {

    @Test
    public void pushShouldWorkEmptyStack() {
        Stack stack = new Stack();
        int numberOfPushes = 6;
        for (int i = 0; i < numberOfPushes; i ++) {
            stack.push(i);
        }
        assertFalse(stack.isEmpty());
        assertEquals(numberOfPushes, stack.size());

    }

    @Test
    public void popShouldWork() {
        Stack stack = new Stack();
        int numberOfPushes = 6;
        for (int i = 0; i < numberOfPushes; i ++) {
            stack.push(i);
        }
        assertEquals(5, stack.pop());
        assertEquals(numberOfPushes - 1, stack.size());
    }

    @Test
    public void peekShouldWork() {
        Stack stack = new Stack();
        int numberOfPushes = 6;
        for (int i = 0; i < numberOfPushes; i ++) {
            stack.push(i);
        }
        assertEquals(5, stack.peek());
        assertEquals(numberOfPushes, stack.size());
    }

    @Test
    public void isEmptyTestTrue() {
        Stack stack = new Stack();
        assertTrue(stack.isEmpty());
    }

    @Test
    public void isEmptyTestFalse() {
        // test after push method
        Stack stack = new Stack();
        int numberOfPushes = 6;
        for (int i = 0; i < numberOfPushes; i++) {
            stack.push(i);
        }
        assertFalse(stack.isEmpty());

    }

    @Test
    public void sizeIsZero() {
        Stack stack = new Stack();
        assertEquals(0, stack.size());
    }

    @Test
    public void sizeIsNotZero() {
        Stack stack = new Stack();
        int numberOfPushes = 6;
        for (int i = 0; i < numberOfPushes; i++) {
            stack.push(i);
        }
        assertEquals(numberOfPushes, stack.size());
    }

}
